-- Active: 1709546088749@@127.0.0.1@3306@Maison
/* the code consists of three SQL statements used to delete specific tables if they already exist in the database*/
Drop Table If EXISTS Recettes;
Drop Table If Exists Categories;
DROP TABLE IF EXISTS User;

/*this command checks if a table named Categories exixts in the database.if it does, the table is deleted (or dropped)*/
CREATE TABLE Categories (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL
    
);


CREATE TABLE Recettes (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    description VARCHAR(900) NOT NULL,
    picture VARCHAR(255) NOT NULL,
    price INTEGER NOT NULL,
    categories_id INTEGER,
    FOREIGN KEY (categories_id) REFERENCES Categories(id)
);


  /*this line does the same for the user table,ensuring it is dropped if it is present in the database*/
  CREATE TABLE User(
    id INT PRIMARY KEY AUTO_INCREMENT,
    email VARCHAR(255) NOT NULL UNIQUE, 
    password VARCHAR(255) NOT NULL,
    role VARCHAR(64) NOT NULL
);


Insert INTO Categories (name) VALUES
('recette'),
('accompagnements'),
('nouveautés');
/*Each has five fields: name,description,price,categories_id,and picture
/example entries include recipes like "pasta cajun" with a description, a price, a category id (likely referencing the categories table), and a url for a picture */

Insert INTO Recettes (name,description,price,categories_id,picture) VALUES
('pasta cajun','Les pâtes de poulet Creamy Cajun font maison sont une recette si simple et rapide,Mélangez 300 g de poulet (ou 200 g de crevettes) avec des épices cajun.Versez 250 ml de crème liquide dans la poêle et ajoutez du parmesan râpé (optionnel).
 parfaite pour les dîners de nuit de semaine. Poitrine de poulet juteuse tossée en pâtes tendres dans une sauce cajun crème ','45',1,'https://i.pinimg.com/564x/bf/75/48/bf75489cce1bc52cf91ba12914bd7c5d.jpg'),
('vegan pasta',' préparé en 20 minutes.Faites cuire 300 g de pâtes (sans œufs) dans eau salée, puis égouttez - les Dans une poêle, faites chauffer 2 cuillères à soupe huile olive,Faites revenir 1 oignon haché, 2 gousses ail,et 2 poivrons en lamelles.
Ajoutez 200 g de champignons tranchés et faites cuire.Incorporez 200 ml de crème végétale (soja, coco ou avoine).
Ajoutez des épices : sel, poivre, paprika ou herbes séchées au choix.
Mélangez les pâtes avec la sauce dans la poêle.
Servez chaud avec du basilic frais ou de la levure nutritionnelle pour remplacer le fromage.
','60',1,'https://i.pinimg.com/564x/db/6b/86/db6b869bd0af54f62cca44810cf5ee7d.jpg'),
('pasta fusilli','Faites cuire 300 g de fusilli dans eau bouillante  salée selon les instructions du paquet. Égouttez-les.Préparez une sauce de votre choix :
Tomate : Faites revenir ail,ajoutez une sauce tomate, des herbes (basilic, origan) et laissez mijoter.Crémeuse : Faites chauffer de la crème avec du parmesan ou une alternative végétale. Ajoutez des épices et des légumes si souhaité.Mélangez les fusilli avec la sauce dans une poêle chaude pour bien les enrober.
Servez chaud, garni de fromage râpé,herbes fraiches ou une touche huile olive  ','55',1,'https://i.pinimg.com/564x/e0/70/a9/e070a92d42015a4aa5e368ec9b542de1.jpg'),
('lasagne bolognaise','Une riche sauce à la viande, une béchamel crémeuse et beaucoup de mozzarella composent cette lasagne ultime','20',1,'https://i.pinimg.com/564x/ae/57/31/ae57318d854e6f36d98a90a96c8dae03.jpg'),
('pates aux thon','Ces pâtes au thon sont simples, express et très savoureuses. Avec une sauce onctueuse, elles sont parfaites pour un repas sans prise de tête.','92',1,'https://i.pinimg.com/564x/39/2b/7d/392b7d210b63793ada0f002c9b11ce9b.jpg'),
('spaghetti carbonnara','Petit voyage en Italie Faites cuire 400 g de spaghettis dans eau bouillante Faites revenir 150 g de lardons ou de guanciale dans une poêle. Dans un bol, mélangez 3 jaunes oeuf  1 œuf entier, et 50 g de parmesan râpé. Ajoutez du poivre noir moulu. Incorporez les spaghettis chauds aux lardons dans la poêle hors du feu.
Ajoutez le mélange œufs-parmesan et un peu eau de cuisson  pour créer une sauce crémeuse. Servez immédiatement, garni de parmesan supplémentaire et de poivre noir.','35',1,'https://i.pinimg.com/564x/fe/07/27/fe072747470c09eeadfcac3f450a130e.jpg'),
('pates aux saumon fumé','Voici une petite recette simple et rapide à réaliser:Faites cuire 300 g de pâtes (tagliatelles ou penne) dans eau bouillante salée Dans une poêle, faites revenir 1 échalote hachée avec un peu de beurre Ajoutez 200 g de saumon fumé coupé en lanières et faites-le chauffer légèrement.
Incorporez 200 ml de crème liquide (entière ou légère) et laissez mijoter quelques minutes.
Assaisonnez avec du poivre et, si désiré, un peu de jus de citron Mélangez les pâtes avec la sauce au saumon dans la poêle.
Servez chaud,  de zeste de citron. ','40',1,'https://i.pinimg.com/564x/8d/3a/f1/8d3af1cce75e09fa7e3855cadec1efab.jpg'),
('pates aux crevettes','6 mars 2022 - Cette épingle a été créée par Just a Taste | Kelly Senyei sur Pinterest. Langoustines rapides aux crevettes','80',1,'https://i.pinimg.com/564x/11/ac/a2/11aca2e148657dda0b2bb7dd0a37aae8.jpg'),
('chocolate strawberry','Le gâteau au chocolat et aux fraises à deux étages le plus simple, garni de ganache au chocolat et de fraises fraîches.','15',2,'https://i.pinimg.com/564x/41/e8/ed/41e8ed5e05821451d922af4a7e7d7dee.jpg'),
('cheesecake chocolate','Cheesecake au chocolat étagé avec croûte Oreo - gâteau crémeux, moelleux et très délicieux ! Parfait pour toutes les occasions et si facile et rapide à préparer - Gâteau au fromage au chocolat en couches avec croûte Oreo - Sans cuisson !','25',2,'https://i.pinimg.com/564x/60/7e/3a/607e3a7fb651401db306d512dbe006f5.jpg'),
('cheesecake speculoos','Un cheesecake au spéculoos frais et crémeux qui ne nécessite pas de cuisson, avec une base de biscuits émiettés, un crémeux aux St Môret et mascarpone et un nappage de pâte de spéculoos.','30',2,'https://i.pinimg.com/564x/b6/c7/89/b6c78981f712a363b359d41ed80d414b.jpg'),
('chocolate Rasberry','Les cupcakes au chocolat et aux framboises sont décadents et délicieux ! Cupcakes moelleux au chocolat noir fourrés,tourbillonnés de glaçage à la crème au beurre et à la framboise, arrosés  avec une framboise fraîche. Ces cupcakes seraient parfaits pour la Saint-Valentin ou toute autre occasion spéciale','20',2,'https://i.pinimg.com/564x/84/f4/80/84f480e3e89c5338f35d23e28bbc6cb6.jpg'),
('tiramisu strawberry','Le tiramisu aux fraises est une délicieuse variante du dessert classique. Fabriqué sans œufs crus','10',2,'https://i.pinimg.com/736x/d3/a2/b5/d3a2b50dba3e0eb1ad7ad0b8a8529617.jpg'),
('banane caramel','Ce dessert à la crème et au caramel et à la banane est tout simplement des desserts les plus délicieux qui soient ! Doux, crémeux, croquant... ce dessert a tout pour plaire !','25',2,'https://i.pinimg.com/564x/b1/2a/07/b12a07c71c2c0ee568cfefb0b508f8c5.jpg'),
('charlotte kinder','Une délicieuse charlotte au Kinder assez légère','10',2,'https://i.pinimg.com/564x/f1/2d/34/f12d3411a98b08aa8f476d109e5094fc.jpg'),
('pain au chocolat','Couche après couche de pâte feuilletée légère et beurrée remplie de chocolat riche et arrosée de plus de chocolat, ces croissants au chocolat faits à partir de zéro sont tout simplement époustouflants ! Pas besoin de plier le beurre ou de refroidir la pâte plusieurs fois !','66',2,'https://i.pinimg.com/564x/c6/64/af/c664afebc15863d1a22bb540b9296d85.jpg'),
('pizza napolitaine','La pizza napolitaine maison. Une recette simple, efficace pour une vraie pizza napolitaine à la maison comme à Naples sans feu de bois !','10',3,'https://i.pinimg.com/564x/ec/a2/53/eca25393445ccac40518b2ed83e7ba0c.jpg'),
('un fruit de mer','Un plateau de fruits de mer','5',3,'https://i.pinimg.com/564x/f9/17/c9/f917c9401e1af2273b7758251634598b.jpg'),
('tenders','poulet chouchoutés pendant 12 heures dans leur petite marinade à base epices cajun','25',3,'https://i.pinimg.com/564x/64/c6/c3/64c6c3752329400aa4ecac9dabf2bc38.jpg'),
('burrito au boeuf et au fromage','Recette de Burrito au Bœuf et au Fromage','30',3,'https://i.pinimg.com/564x/c8/e2/ac/c8e2ac94b991a84a01b76d56957b8ba9.jpg'),
('cheeseburger burritos','Ces burritos au cheeseburger sont une recette très populaire chez nous depuis des années. Tout le monde les aime et en redemande !!','45',3,'https://i.pinimg.com/736x/5b/fb/83/5bfb83c320b092512042a52c662342a1.jpg'),
('poulet maffe','voici le plat que je vous propose de partager avec vous ici au poulet et servi avec du riz blanc et cette sauce parfumée. Préparez la viande : Faites revenir 500 g de bœuf ou de poulet en morceaux dans une marmite avec un peu huile . Ajoutez les légumes : Incorporez des morceaux de carottes, patates douces, choux et/ou manioc, et faites-les revenir brièvement.Préparez la sauce : Diluez 3-4 cuillères à soupe de pâte arachide dans eau chaude,  puis ajoutez-la dans la marmite avec 2 tomates fraîches râpées ou du concentré de tomate. Assaisonnez : Ajoutez 1 oignon émincé, 2 gousses d’ail, un cube bouillon, du sel, du poivre, et du piment selon votre goût. Cuisinez à feu doux : Ajoutez eau pour couvrir les ingrédients et laissez mijoter pendant environ 45 minutes,pour que la viande soit tendre et la sauce épaissie Servez : Accompagnez le mafé de riz blanc bien chaud.  ','35',3,'https://i.pinimg.com/564x/6a/e0/63/6ae0639601dfeb553738d9e65287c726.jpg'),
('pastel','les empanadas de la Sardaigne : tous les détails à préparer à la maison','40',3,'https://i.pinimg.com/564x/cf/93/c4/cf93c4f61a2e4e521c991010efc48c34.jpg'),
('macaroni','Les macaronis sont sautés avec des oignons, des pois et du bœuf haché dans une sauce riche en umami.','15',3,'https://i.pinimg.com/564x/6e/bd/aa/6ebdaa9e215ba148fb8e017369519b84.jpg');


/* the second part inserts records into to the user table,which stores information about application users 
for example ,there is an admin user with the email "cheikh@test.com" and a hashed password, along with two additionnal users  with different roles (role_admin and role_user) */
INSERT INTO User (email,password,role) VALUES 
('cheikh@test.com', '$2y$10$ExbjqPDMeGm1lZ6GiaDgO.YZqvDucH5TxYjrK2jo1JxI3V0RbHwvq', 'ROLE_ADMIN'),
('admin@test.com', 'passer@123', 'ROLE_USER'),
('test@test.com', '$2y$10$ExbjqPDMeGm1lZ6GiaDgO.YZqvDucH5TxYjrK2jo1JxI3V0RbHwvq', 'ROLE_USER');



