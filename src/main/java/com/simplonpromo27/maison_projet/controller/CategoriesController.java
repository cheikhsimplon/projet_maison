package com.simplonpromo27.maison_projet.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.simplonpromo27.maison_projet.entity.Categories;
import com.simplonpromo27.maison_projet.repository.CategoriesRepository;

import jakarta.validation.Valid;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/categories")

public class CategoriesController {
  @Autowired
    private CategoriesRepository categoriesRepo;

    @GetMapping
    public List<Categories> all() {
        return categoriesRepo.findAll();
    }

    @GetMapping("/{id}")
    public Categories one(@PathVariable int id) {
        Categories categories = categoriesRepo.findById(id);
        if(categories == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return categories;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Categories add( @Valid @RequestBody Categories categories) {
        categoriesRepo.persist(categories);
        return categories;
    }
  @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable int id) {
        one(id); 
        categoriesRepo.delete(id);
    }

    @PutMapping("/{id}")
    public Categories replace(@PathVariable int id, @Valid @RequestBody Categories categories) {
        one(id); 
        categories.setId(id); 
        categoriesRepo.update(categories);
        return categories;
    }

}
