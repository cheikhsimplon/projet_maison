package com.simplonpromo27.maison_projet.controller;


    import org.springframework.beans.factory.annotation.Autowired;
    import org.springframework.http.HttpStatus;
    import org.springframework.security.core.annotation.AuthenticationPrincipal;
    import org.springframework.security.crypto.password.PasswordEncoder;
    import org.springframework.web.bind.annotation.GetMapping;
    import org.springframework.web.bind.annotation.PostMapping;
    import org.springframework.web.bind.annotation.RequestBody;
    import org.springframework.web.bind.annotation.RestController;
    import org.springframework.web.server.ResponseStatusException;
    
    import com.simplonpromo27.maison_projet.entity.User;
    import com.simplonpromo27.maison_projet.repository.UserRepository;
    import jakarta.validation.Valid;
    
    @RestController
    public class UserController { 
    
        @Autowired
        private UserRepository userRepo;
        
        private PasswordEncoder hasher;

        @Autowired
        private UserRepository repo;
    
        @PostMapping("/api/user")
        public User register(@Valid @RequestBody User user) {
    
            if(repo.findByEmail(user.getEmail()).isPresent()) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User already exists");
            }
            user.setRole("ROLE_USER");
            repo.persist(user);
            return user;
        }
    
    
        @GetMapping("/api/account")
        public User myAccount(@AuthenticationPrincipal User user) {
            return user;
        }
    }
    

