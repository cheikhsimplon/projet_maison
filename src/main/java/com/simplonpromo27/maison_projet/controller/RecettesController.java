package com.simplonpromo27.maison_projet.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;


import com.simplonpromo27.maison_projet.entity.Recettes;


import com.simplonpromo27.maison_projet.repository.RecettesRepository;

import jakarta.validation.Valid;
import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.geometry.Positions;

@CrossOrigin("*")
@RequestMapping("/api/recettes")

@RestController
public class RecettesController {


     
    @Autowired
    private RecettesRepository recettesRepo;                                                          

    @GetMapping
    public List<Recettes> all() {
        return recettesRepo.findAll();
    }

    @GetMapping("/{id}")
    public Recettes one(@PathVariable int id) {
        Recettes recettes = recettesRepo.findById(id);
        if(recettes == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return recettes;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Recettes add( @Valid @RequestPart Recettes recettes, @RequestPart MultipartFile src) {
        String renamed = UUID.randomUUID() + ".jpg";
        try {
            Thumbnails.of(src.getInputStream())
                    .width(900)
                    .toFile(new File(getUploadFolder(), renamed));
            Thumbnails.of(src.getInputStream())
                    .size(200, 200)
                    .crop(Positions.CENTER)
                    .toFile(new File(getUploadFolder(), "thumbnail-" + renamed));
        } catch (IOException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Upload error", e);
        }

        recettes.setPicture(renamed);
        recettesRepo.persist(recettes);
        return recettes;
    }

    private File getUploadFolder() {
        File folder = new File(getClass().getClassLoader().getResource(".").getPath().concat("static/uploads"));
        if (!folder.exists()) {
            folder.mkdirs();
        }
        return folder;

    }

@DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable int id) {
        one(id); 
        recettesRepo.delete(id);
    }

    @PutMapping("/{id}")
    public Recettes replace(@PathVariable int id, @Valid @RequestBody Recettes recettes) {
        one(id); 
        recettes.setId(id); 
        recettesRepo.update(recettes);
        return recettes;
    } 
    @GetMapping("/categories/{id}")
    public List<Recettes> getRecettesByCategorie(@PathVariable int id) {
        List<Recettes> recettes = recettesRepo.findByCategorieId(id);
    
        if (recettes.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Aucun recette trouvé pour cette catégorie");
        }
    
        return recettes;
    }
    
        
}