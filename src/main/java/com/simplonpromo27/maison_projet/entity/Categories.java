package com.simplonpromo27.maison_projet.entity;

import jakarta.validation.constraints.NotBlank; // Import for validation annotations

public class Categories {
    
    // The unique identifier for each category
    private Integer id;
    @NotBlank 
    private String name;
    // The ID of the related recipe (foreign key)
    private int recette_Id;
    
    // Default constructor 
    public Categories() {
    }

    // Constructor without ID 
    public Categories(@NotBlank String name, int recette_Id) {
        this.name = name;
        this.recette_Id = recette_Id;
    }

    // Constructor with ID 
    public Categories(Integer id, @NotBlank String name, int recette_Id) {
        this.id = id;
        this.name = name;
        this.recette_Id = recette_Id;
    }

    // Getter and setters
    public Integer getId() {
        return id;
    }

    
    public void setId(Integer id) {
        this.id = id;
    }

    
    public String getName() {
        return name;
    }

   
    public void setName(String name) {
        this.name = name;
    }

    
    public int getRecette_Id() {
        return recette_Id;
    }

    
    public void setRecette_Id(int recette_Id) {
        this.recette_Id = recette_Id;
    }
}
