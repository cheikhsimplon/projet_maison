package com.simplonpromo27.maison_projet.entity;

/*This Java code imports various classes and interfaces that are needed for building 
a user authentication and validation system */
import java.util.Collection;
import java.util.List;
/*
These imports bring in Java's Collection and List interfaces from the java.util package, 
which are used to manage collections of objects, such as lists of user roles or authorities. */
/*
This import brings in Hibernate’s @Length annotation, which can be used to set length constraints on fields 
( restricting the length of a username or password). */
import org.hibernate.validator.constraints.Length;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
/*UserDetails is a core interface in Spring Security. It represents a user's information and contains methods to get the username,
 password, authorities (permissions), and account status. It is essential for managing and authenticating users. */

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
/*
The @NotBlank annotation ensures that a string field is not null and is not empty. It’s commonly used to validate required fields like usernames or passwords in a user registration form. */



/*This User class implements the UserDetails interface from Spring Security, which is essential for managing user authentication and authorization */
public class User implements UserDetails {

/*
Stores the unique identifier for the user. It’s usually auto-generated when the user is saved in a database. 

@Email: Ensures the email field contains a valid email format.
@NotBlank: Ensures the email is not null or empty.
email stores the user’s email address, which is also typically the username in authentication.
*/

    private Integer id;
    @Email
    @NotBlank
    private String email;
    @JsonProperty(access = Access.WRITE_ONLY)
    @Length(min= 4)
    private String password;
    private String role;

    public User(String email, String password, String role) {
        this.email = email;
        this.password = password;
        this.role = role;
    }
    public User(Integer id, String email, String password, String role) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.role = role;
    }
    public User() {
    }

/*The User class now includes getter and setter methods for each attribute (id, email, password, and role).
 *  This is typically used when a user is retrieved from a database where the id is generated automatically.
 * 
getEmail(): Retrieves the user’s email address, which often serves as the username for authentication.
 */ 

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getRole() {
        return role;
    }
    public void setRole(String role) {
        this.role = role;
    }

    /*This code snippet in the User class implements the UserDetails interface from Spring Security,
     which makes it compatible with Spring's authentication system
   Returns the email as the identifier for this user, which is used by Spring Security as the username during authentication.
   In this setup, email is both the login identifier and a unique attribute. 
    */
    @JsonProperty(access = Access.READ_ONLY)
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(
            new SimpleGrantedAuthority(role)
        );
    }
    @Override
    public String getUsername() {
        return email;
    }
    @Override
    public boolean isAccountNonExpired() {
        return true;    
    }
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }
    @Override
    public boolean isEnabled() {
        return true;
    }
}
