package com.simplonpromo27.maison_projet.entity;

import org.hibernate.validator.constraints.Length;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PositiveOrZero;

/*The recettes class contains five attributes 
 * the class has two  constructors: constructors without id : used for creating new recipes without specifying an id,usually
 * before they are saved in a database where an id will be assigned.
 * constructor with id : used when creating a recipe object with all attributes, including the id  ( such as when loading data from a database  )
 * the constructors allow the creation of recettes object either with or without an id, depending on the application's needs 
*/


public class Recettes {

    private Integer id;
    @NotBlank @Length(max=45)
    private String name;
    @NotBlank
    private String description;
    @NotNull
    @PositiveOrZero
    private Integer price;
    private String picture;
    private Integer categoriesId;

    public Recettes() {
    }

    // Constructeur with id
    public Recettes(Integer id, String name, String description, Integer price, String picture, Integer categoriesId) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.picture = picture;
        this.categoriesId = categoriesId;
    }

    // Constructeur without id
    public Recettes(String name, String description, Integer price, String picture, Integer categoriesId) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.picture = picture;
        this.categoriesId = categoriesId;
    }

    // Getters and  setters
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Integer getCategoriesId() {
        return categoriesId;
    }

    public void setCategoriesId(Integer categoriesId) {
        this.categoriesId = categoriesId;
    }
    public String getLink() {
        if(picture.startsWith("http")) {
            return picture;
        }
        final String baseUrl = 
ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString();
        return baseUrl+"/uploads/"+picture;
    }

    public String getThumbnailLink() {
        if(picture.startsWith("http")) {
            return picture;
        }
        final String baseUrl = 
ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString();
        return baseUrl+"/uploads/thumbnail-"+picture;
    }

}



/*this code definies a set of getter and setter methods for the recettes class.
 * These methods are used to retrieve (or "get") the values of private attributes.
the purpose of these methods is to provide controlled access to the private attributes
of the Recette class 
 */





