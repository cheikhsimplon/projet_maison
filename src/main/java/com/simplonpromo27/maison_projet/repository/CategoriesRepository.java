package com.simplonpromo27.maison_projet.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.simplonpromo27.maison_projet.entity.Categories;

@Repository
public class CategoriesRepository {
    @Autowired
    private DataSource dataSource;

    public List<Categories> findAll() {
        List<Categories> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM Categories");
            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                list.add(sqlToCategories(result));
            }   
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return list;
    }

    public Categories findById(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM Categories WHERE id = ?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return sqlToCategories(result);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return null;
    }

    public boolean delete(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM Categories WHERE id = ?");
            stmt.setInt(1, id);
            int result = stmt.executeUpdate();
            return result > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
    }

    public boolean persist(Categories categories) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                "INSERT INTO Categories (name, recette_id) VALUES (?, ?)", 
                Statement.RETURN_GENERATED_KEYS
            );
            stmt.setString(1, categories.getName());
            stmt.setInt(2,  categories.getRecette_Id());

            if (stmt.executeUpdate() == 1) {
                ResultSet keys = stmt.getGeneratedKeys();
                if (keys.next()) {
                    categories.setId(keys.getInt(1));
                    return true;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return false;
    }

    private Categories sqlToCategories(ResultSet result) throws SQLException {
        int id = result.getInt("id");
        String name = result.getString("name");
        int recette_Id = result.getInt("recette_id"); 
        return new Categories(id, name, recette_Id);
    }



    public boolean update(Categories categories) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "UPDATE categorie SET nom = ? WHERE id = ?");
            stmt.setString(1, categories.getName());
            stmt.setInt(2, categories.getId());
            if (stmt.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    
    }
   

   
    






