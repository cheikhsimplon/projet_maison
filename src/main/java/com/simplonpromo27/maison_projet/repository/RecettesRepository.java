package com.simplonpromo27.maison_projet.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.simplonpromo27.maison_projet.entity.Recettes;
@Repository
public class RecettesRepository {
    @Autowired
    private DataSource dataSource; // Injecting the DataSource to manage database connections.

    // Retrieves all recipes from the database.
    public List<Recettes> findAll() {
        List<Recettes> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) { // Establishing a connection with the database.
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM Recettes"); // Preparing SQL query.
            ResultSet result = stmt.executeQuery(); 

            while (result.next()) {
                list.add(sqlToRecettes(result)); // Mapping each row to a Recettes object.
            }
        } catch (SQLException e) {
            e.printStackTrace(); // Logging the exception for debugging purposes.
            throw new RuntimeException("Error in repository", e); // Wrapping and throwing a runtime exception.
        }
        return list; // Returning the list of recipes.
    }

    // Retrieves a recipe by its ID.
    public Recettes findById(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM Recettes WHERE id=?"); // Preparing query with parameter.
            stmt.setInt(1, id); // Binding the ID parameter.
            ResultSet result = stmt.executeQuery(); // Executing the query.

            if (result.next()) {
                return sqlToRecettes(result); // Mapping the result to a Recettes object.
            }
        } catch (SQLException e) {
            e.printStackTrace(); 
            throw new RuntimeException("Error in repository", e); // Handling SQL errors.
        }
        return null; // Returning null if no recipe is found.
    }

    // Converts a ResultSet row into a Recettes object.
    private Recettes sqlToRecettes(ResultSet result) throws SQLException {
        // Extracts the data from the result and creates a Recettes object.
        int categoriesId = result.getInt("categories_id"); // Retrieves the category ID from the row.
        return new Recettes(
                result.getInt("id"), 
                result.getString("name"), 
                result.getString("description"), 
                result.getInt("price"), 
                result.getString("picture"), 
                categoriesId // Category ID of the recipe.
        );
    }

    // Inserts a new recipe into the database.
    public boolean persist(Recettes recettes) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                "INSERT INTO Recettes (name, description, price, picture, categories_id) VALUES (?, ?, ?, ?, ?)", 
                Statement.RETURN_GENERATED_KEYS // Enables retrieval of the auto-generated ID.
            );
            stmt.setString(1, recettes.getName()); 
            stmt.setString(2, recettes.getDescription()); 
            stmt.setInt(3, recettes.getPrice()); 
            stmt.setString(4, recettes.getPicture()); 
            stmt.setInt(5, recettes.getCategoriesId()); // Binding the category ID.

            if (stmt.executeUpdate() == 1) { // Executes the insert query.
                ResultSet keys = stmt.getGeneratedKeys(); // Retrieves the auto-generated key.
                if (keys.next()) {
                    recettes.setId(keys.getInt(1)); // Sets the generated ID to the recipe object.
                    return true; // Returns true if insertion is successful.
                }
            }
        } catch (SQLException e) {
            e.printStackTrace(); // Logs any SQL error.
            throw new RuntimeException("Error in repository", e); 
        }
        return false; // Returns false if the insertion fails.
    }

    // Updates an existing recipe in the database.
    public boolean update(Recettes recettes) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "UPDATE Recettes SET name = ?, description = ?, price = ?, picture = ?, categories_id = ? WHERE id = ?");
            stmt.setString(1, recettes.getName()); 
            stmt.setString(2, recettes.getDescription()); 
            stmt.setInt(3, recettes.getPrice()); 
            stmt.setString(4, recettes.getPicture()); 
            stmt.setInt(5, recettes.getCategoriesId()); 
            stmt.setInt(6, recettes.getId()); 

            if (stmt.executeUpdate() == 1) { // Executes the update query.
                return true; // Returns true if the update is successful.
            }
        } catch (SQLException e) {
            e.printStackTrace(); 
            throw new RuntimeException("Error in repository", e); // Wraps and throws a runtime exception.
        }
        return false; // Returns false if the update fails.
    }

    // Deletes a recipe by its ID.
    public boolean delete(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM Recettes WHERE id=?");
            stmt.setInt(1, id); // Binding the recipe ID.

            if (stmt.executeUpdate() == 1) { // Executes the delete query.
                return true; // Returns true if the deletion is successful.
            }
        } catch (SQLException e) {
            e.printStackTrace(); // Logs any SQL error.
            throw new RuntimeException("Error in repository", e); // Wraps and throws a runtime exception.
        }
        return false; // Returns false if the deletion fails.
    }

    // Retrieves recipes by their category ID.
    public List<Recettes> findByCategorieId(int categorieId) {
        List<Recettes> recettesList = new ArrayList<>();
        String sql = "SELECT * FROM Recettes WHERE categories_id = ?"; // Query to filter recipes by category.

        try (Connection connection = dataSource.getConnection();
             PreparedStatement stmt = connection.prepareStatement(sql)) {
            stmt.setInt(1, categorieId); 
            ResultSet resultSet = stmt.executeQuery(); 

            while (resultSet.next()) {
                recettesList.add(sqlToRecettes(resultSet)); // Maps each result row to a Recettes object.
            }
        } catch (SQLException e) {
            e.printStackTrace(); 
            throw new RuntimeException("Error in repository", e); 
        }

        return recettesList; // Returns the list of recipes in the given category.
    }
}

